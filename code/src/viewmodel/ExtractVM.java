package viewmodel;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.Extract;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class ExtractVM implements PropertyChangeListener {
    private final Extract model;

    private StringProperty extract = new SimpleStringProperty();
        public String getExtract() { return extract.get(); }
        public StringProperty extractProperty() { return extract; }
        public void setExtract(String extract) { this.extract.set(extract); }

    public ExtractVM(Extract model) {
        this.model = model;
        model.addListener(this);

        extract.addListener((u1, u2, newValue) -> model.setExtract(newValue));
        extract.set(model.getExtract());
    }

    public Extract getModel() {
        return model;
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getOldValue() != event.getNewValue()) {
            if (event.getPropertyName().equals(Extract.PROP_EXTRACT)) {
                extract.set((String) event.getNewValue());
            }
        }
    }
}
