package viewmodel.libraryvm;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.library.Library;
import model.work.Novel;
import viewmodel.workvm.NovelVM;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class LibraryVM implements PropertyChangeListener {
    private final Library model;

    private final ObservableList<NovelVM> novelsObs = FXCollections.observableArrayList();
    private final ListProperty<NovelVM> novels = new SimpleListProperty<>(novelsObs);
        public ObservableList<NovelVM> getNovels() { return novels.get(); }
        public ListProperty<NovelVM> novelsProperty() { return novels; }
        public void setNovels(ObservableList<NovelVM> novels) { this.novels.set(novels); }

    public LibraryVM(Library library) {
        model = library;

        model.getNovels().forEach(novel -> novels.add(new NovelVM(novel)));

        model.addListener(this);
    }

    public void addNovel(Novel novel) {
            model.addNovel(novel);
    }

    public void removeNovel(NovelVM novelVM) {
            model.removeNovel((Novel) novelVM.getModel());
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getOldValue() != event.getNewValue() && event.getOldValue() == null) {
            if (event.getPropertyName().equals(Library.PROP_NOVELS)) {
                novels.add(new NovelVM((Novel) event.getNewValue()));
            }
        }

        if (event.getOldValue() != event.getNewValue() && event.getNewValue() == null) {
            if (event.getPropertyName().equals(Library.PROP_NOVELS)) {
                novels.remove(((IndexedPropertyChangeEvent) event).getIndex());
            }
        }
    }
}
