package viewmodel.workvm;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import model.work.Work;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public abstract class WorkVM implements PropertyChangeListener {

    private final Work model;

    private final StringProperty title = new SimpleStringProperty();
        public StringProperty titleProperty() { return title; }
        public String getTitle() { return title.get(); }
        public void setTitle(String title) { this.title.set(title); }

    private final StringProperty cover = new SimpleStringProperty();
        public String getCover() { return cover.get(); }
        public StringProperty coverProperty() { return cover; }
        public void setCover(String cover) { this.cover.set(cover); }

    public WorkVM(Work work) {
        model = work;

        setTitle(model.getTitle());
        setCover(model.getCover());

        title.addListener((u1, u2, newValue) -> model.setTitle(newValue));
        cover.addListener((u1, u2, newValue) -> model.setCover(newValue));

        model.addListener(this);
    }

    public Work getModel() {
        return model;
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        if (event.getOldValue() != event.getNewValue()) {
            if (event.getPropertyName().equals(Work.PROP_TITLE)) {
                title.set((String) event.getNewValue());
            }

            if (event.getPropertyName().equals(Work.PROP_COVER)) {
                cover.set((String) event.getNewValue());
            }
        }
    }
}
