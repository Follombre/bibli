package viewmodel.workvm;

import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.Extract;
import model.work.Novel;
import viewmodel.ExtractVM;

import java.beans.IndexedPropertyChangeEvent;
import java.beans.PropertyChangeEvent;

public class NovelVM extends WorkVM {
    private Novel model;

    private ObservableList<ExtractVM> extractsObs = FXCollections.observableArrayList();
    private ListProperty<ExtractVM> extracts = new SimpleListProperty<>(extractsObs);
        public ObservableList<ExtractVM> getExtracts() { return extracts.get(); }
        public ListProperty<ExtractVM> extractsProperty() { return extracts; }
        public void setExtracts(ObservableList<ExtractVM> extracts) { this.extracts.set(extracts); }

    public NovelVM(Novel novel) {
        super(novel);
        model = novel;
        model.getExtracts().forEach(extract -> extracts.add(new ExtractVM(extract)));
    }

    public void addExtract(Extract e) {
        model.addExtract(e);
    }

    public void removeExtract(Extract e) {
        model.removeExtract(e);
    }

    @Override
    public void propertyChange(PropertyChangeEvent event) {
        super.propertyChange(event);

        if (event.getOldValue() != event.getNewValue()) {
            if (event.getPropertyName().equals(Novel.PROP_EXTRACTS)) {
                if (event.getNewValue() != null) {
                    extracts.add(new ExtractVM((Extract) event.getNewValue()));
                } else if (event.getOldValue() != null) {
                    extracts.remove(((IndexedPropertyChangeEvent)event).getIndex());
                }
            }
        }
    }
}
