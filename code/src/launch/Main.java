package launch;

import data.DeserializerLibrary;
import data.SerializerLibrary;
import data.StubLibrary;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.library.Library;

import java.io.IOException;

public class Main extends Application {
    public static Library library;

    /**
     * Name of the app
     */
    private static final String APP_NAME = "Bibli";

    @Override
    public void start(Stage stage) throws Exception {
        try {
            library = new DeserializerLibrary().load();
        } catch (IOException | ClassNotFoundException e) {
            library = new StubLibrary().load();
        }

        Parent root = FXMLLoader.load(getClass().getResource("/fxml/MainWindow.fxml"));
        stage.setTitle(APP_NAME);
        stage.setScene(new Scene(root));
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        new SerializerLibrary().save(library);
        super.stop();
    }
}
