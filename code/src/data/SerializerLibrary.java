package data;

import model.library.Library;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializerLibrary implements Saver<Library> {
    @Override
    public void save(Library library) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("save.bin"));
        oos.writeObject(library);
    }
}
