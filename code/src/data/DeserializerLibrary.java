package data;

import model.library.Library;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializerLibrary implements Loader<Library> {
    @Override
    public Library load() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream("save.bin"));
        return (Library) ois.readObject();
    }
}
