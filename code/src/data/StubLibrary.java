package data;

import model.library.Library;
import model.work.Novel;

public class StubLibrary implements Loader<Library> {
    @Override
    public Library load() {
        Library library = new Library();
        library.addNovel(new Novel("Un roman 1"));
        library.addNovel(new Novel("Un roman 2"));
        library.addNovel(new Novel("Un roman 3"));
        library.addNovel(new Novel("Un roman 4"));
        library.addNovel(new Novel("Un roman 5"));
        return library;
    }
}
