package view;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.concurrent.Callable;

/**
 * ImageView with Url to bind the URL
 */
public class UrlImageView extends ImageView {
    private Callable<Void> callable;

    private final StringProperty url = new SimpleStringProperty();
        public String getUrl() { return url.get(); }
        public StringProperty urlProperty() { return url; }
        public void setUrl(String url) { this.url.set(url); }

    /**
     * Constructor of the Image View with Url
     */
    public UrlImageView() {
        this.url.addListener((u1, oldUrl, newUlr) -> {
            if (newUlr != null) {
                setIm(newUlr);
            } else {
                this.setImage(null);
            }
        });
    }

    private void setIm(String newUlr) {
        try {
            this.setImage(new Image(newUlr));
        } catch (IllegalArgumentException e) {
            this.setImage(null);
            try {
                callable.call();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }
    }

    public void setErrorCallable(Callable<Void> c) {
        callable = c;
    }
}
