package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import launch.Main;
import model.Extract;
import model.work.Novel;
import viewmodel.ExtractVM;
import viewmodel.libraryvm.LibraryVM;
import viewmodel.workvm.NovelVM;

import java.io.File;
import java.net.MalformedURLException;

public class MainWindow {
    private final LibraryVM libraryVM = new LibraryVM(Main.library);

    @FXML
    private ListView<NovelVM> novelsListView;

    @FXML
    private TextField titleTextField;

    @FXML
    private Label coverLabel;

    @FXML
    private UrlImageView coverImage;

    @FXML
    private ListView<ExtractVM> extractsListView;

    @FXML
    private TextArea extractTextField;

    @FXML
    private void initialize() {
        novelsListView.itemsProperty().bind(libraryVM.novelsProperty());

        setCellFactoryNovel();

        selectItemNovel();

        addListenerExtract();
    }

    private void setCellFactoryNovel() {
        novelsListView.setCellFactory(param -> new ListCell<>() {
            @Override
            protected void updateItem(NovelVM novelVM, boolean empty) {
                super.updateItem(novelVM, empty);
                if (!empty) {
                    textProperty().bind(novelVM.titleProperty());
                } else {
                    textProperty().unbind();
                    setText("");
                }
            }
        });
    }

    private void selectItemNovel() {
        novelsListView.getSelectionModel().selectedItemProperty().addListener((u1, oldValue, newValue) -> {
            if (oldValue != null) {
                unbindNovel(oldValue);
            }

            if (newValue != null) {
                bindNovel(newValue);
            }
        });
    }

    private void unbindNovel(NovelVM oldNovel) {
        titleTextField.textProperty().unbindBidirectional(oldNovel.titleProperty());
        titleTextField.setText("");
        coverLabel.textProperty().unbind();
        coverLabel.setText("");
        coverImage.urlProperty().unbind();
        coverImage.setUrl(null);
        extractsListView.itemsProperty().unbind();
        extractsListView.setItems(null);
    }

    private void bindNovel(NovelVM newNovel) {
        titleTextField.textProperty().bindBidirectional(newNovel.titleProperty());
        coverLabel.textProperty().bind(newNovel.coverProperty());
        coverImage.setErrorCallable(() -> {
            System.err.println("ERROR: Invalid URL or ressource not found");
            return null;
        });
        coverImage.urlProperty().bind(newNovel.coverProperty());

        extractsListView.itemsProperty().bind(newNovel.extractsProperty());
        setCellFactoryExtract();
    }

    private void setCellFactoryExtract() {
        extractsListView.setCellFactory(param -> new ListCell<>() {
            @Override
            protected void updateItem(ExtractVM extractVM, boolean empty) {
                super.updateItem(extractVM, empty);
                if (!empty) {
                    textProperty().bind(extractVM.extractProperty());
                } else {
                    textProperty().unbind();
                    textProperty().set("");
                }
            }
        });
    }

    private void addListenerExtract() {
        extractsListView.getSelectionModel().selectedItemProperty().addListener((u1, oldExtract, newExtract) -> {
            if (oldExtract != null) {
                extractTextField.textProperty().unbindBidirectional(oldExtract.extractProperty());
                extractTextField.setText("");
            }

            if (newExtract != null) {
                extractTextField.textProperty().bindBidirectional(newExtract.extractProperty());
            }
        });
    }

    @FXML
    private void selectImageButton(ActionEvent event) {
        FileChooser fc = new FileChooser();
        fc.setTitle("Open Image File");
        fc.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg", "*.gif"));

        selectInitialDirectory(fc);

        selectFile(event, fc);
    }

    private void selectInitialDirectory(FileChooser fc) {
        String os = System.getProperty("os.name");
        if (os.startsWith("Linux")) {
            fc.setInitialDirectory(new File(System.getProperty("user.home")));
        } else if (os.startsWith("Windows")) {
            fc.setInitialDirectory(new File(System.getProperty("user.home"))); // To be verified
        } // Mac OS is to do
    }

    private void selectFile(ActionEvent event, FileChooser fc) {
        File file = fc.showOpenDialog(((Node) event.getTarget()).getScene().getWindow());
        if (file != null) {
            try {
                novelsListView.getSelectionModel().getSelectedItem().setCover(file.toURI().toURL().toString());
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    private void addNovelButton() {
        libraryVM.addNovel(new Novel("Un nouveau roman"));
    }

    @FXML
    private void removeNovelButton() {
        libraryVM.removeNovel(novelsListView.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void addExtractButton() {
        novelsListView.getSelectionModel().getSelectedItem().addExtract(new Extract("Un extrait"));
    }

    @FXML
    private void removeExtractButton() {
        novelsListView.getSelectionModel().getSelectedItem().removeExtract(extractsListView.getSelectionModel().getSelectedItem().getModel());
    }
}
