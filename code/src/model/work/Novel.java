package model.work;

import model.Extract;

import java.util.ArrayList;
import java.util.List;

/**
 * Represent a novel
 */
public class Novel extends Work {
    /**
     * List of different extracts
     */
    private final List<Extract> extracts;
    public static final String PROP_EXTRACTS = "PROP_EXTRACTS";

    public Novel(String title) {
        super(title);
        extracts = new ArrayList<>();
    }

    public List<Extract> getExtracts() {
        return extracts;
    }

    public void addExtract(Extract e) {
        extracts.add(e);
        getSupport().fireIndexedPropertyChange(PROP_EXTRACTS, extracts.size()-1, null, e);
    }

    public void removeExtract(Extract e) {
        int index = extracts.indexOf(e);
        extracts.remove(e);
        getSupport().fireIndexedPropertyChange(PROP_EXTRACTS, index, e, null);
    }
}
