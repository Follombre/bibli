package model.work;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

/**
 * Represent a general work of an author
 */
public abstract class Work implements Serializable {

    private transient PropertyChangeSupport support;

    /**
     * Title of the work
     */
    private String title;
    public static final String PROP_TITLE = "PROP_TITLE";

    /**
     * Link to the cover of the work
     */
    private String cover;
    public static final String PROP_COVER = "PROP_COVER";

    public Work(String title) {
        this.title = title;
    }

    public PropertyChangeSupport getSupport() {
        if (support == null) {
            support = new PropertyChangeSupport(this);
        }
        return support;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        String oldTitle = this.title;
        this.title = title;
        getSupport().firePropertyChange(PROP_TITLE, oldTitle, title);
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        String oldCover = this.cover;
        this.cover = cover;
        getSupport().firePropertyChange(PROP_COVER, oldCover, cover);
    }

    public void addListener(PropertyChangeListener prop) {
        getSupport().addPropertyChangeListener(prop);
    }
}
