package model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

public class Extract implements Serializable {
    private transient PropertyChangeSupport support;

    private String extract;
    public static final String PROP_EXTRACT = "PROP_EXTRACT";

    public Extract(String s) {
        extract = s;
    }

    public PropertyChangeSupport getSupport() {
        if (support == null) {
            support = new PropertyChangeSupport(this);
        }
        return support;
    }

    public String getExtract() {
        return extract;
    }

    public void setExtract(String extract) {
        String oldExtract = this.extract;
        this.extract = extract;
        getSupport().firePropertyChange(PROP_EXTRACT, oldExtract, extract);
    }

    public void addListener(PropertyChangeListener prop) {
        getSupport().addPropertyChangeListener(prop);
    }
}
