# Bibli, a directory for your library

## Description
This app allowing you to list the contents of your library (novels, comics, etc.)

## Screenshots
Coming soon

## Features
### You can now list your :
* [ ]  CDs
* [ ]  Comics
* [ ]  DVDs
* [ ]  Mangas
* [X]  Novels

## Download
The application is not available for download.

But if you wish, you can build the application. You need Java and JavaFX 14.

## Warning
Please, note that this application is under development and that it still lacks a lot of features as well as a good interface and that bugs may appear when using it.
The application is in the early stages of development. This means that there can be many bugs.